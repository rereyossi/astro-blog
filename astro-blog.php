<?php
/**
 * Plugin Name: Astro Blog
 * Description: Blog Widgets for Elementor page builder.
 * Plugin URI:  https://webforia.id/astro-blog
 * Version:     1.2.1
 * Author:      Webforia
 * Author URI:  https://webforia.id
 * Text Domain: astro-blog
 */

if (!defined('ABSPATH')) {
    exit;
}

define('ASTRO_BLOG_TEMPLATE', plugin_dir_path(__FILE__));
define('ASTRO_BLOG_ASSETS', plugin_dir_url(__FILE__));

// Require the main plugin file
include plugin_dir_path(__FILE__) . 'function.php'; // function framework
include plugin_dir_path(__FILE__) . 'core/core-init.php'; // class framework
include plugin_dir_path(__FILE__) . 'widgets/elementor-init.php'; // class framework
include plugin_dir_path(__FILE__) . 'plugin.php'; // final class

$check_update = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/rereyossi/astro-blog', __FILE__, 'astro-blog');
$check_update->setBranch('stable_release');

/*=================================================;
/* PRODUCT VAR
/*================================================= */
function astro_blog_var($value, $arg = '')
{
    $default = apply_filters('astro_blog_var', array(
        'product-name' => 'Astro Blog',
        'product-slug' => 'astro-blog',
        'product-version' => '1.2.0',
        'product-docs' => 'https://www.panduan.webforia.id/astro-blog',
        'pruduct-url' => 'https://www.webforia.id/astro-blog',
        'product-group' => 'https://web.facebook.com/groups/265676900996871',
        'product-support' => 'https://web.facebook.com/groups/265676900996871',
    )
    );

    return !empty($default[$value]) ? $default[$value] . $arg : '';

}
