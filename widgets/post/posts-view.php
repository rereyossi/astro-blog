<?php $post_classes = !empty($settings['class'])?$settings['class']:"";?>


<div class="flex-item">

	<div id="<?php echo 'post-'.get_the_ID()?>" class="<?php echo $post_classes?>">


	<?php if (has_post_thumbnail() && $settings['image_size'] != 'none'): ?>
		<div class="at-post__thumbnail rt-img rt-img--full">
			<?php the_post_thumbnail($settings['image_size'], 'img-responsive')?>
		</div>
	<?php endif;?>

	<div class="at-post__body">

		<?php if (get_the_category() && $settings['meta_category']):?>
			<div class="at-post__badges">
				<?php foreach (get_the_category() as $term):?>
				<a href="<?php echo get_category_link($term->term_id)?>" class="<?php echo $term->slug?>" ><?php echo $term->name?></a>
				<?php endforeach?>
			</div>
		<?php endif?>

		<h3 class="at-post__title"><a href="<?php the_permalink( )?>"><?php the_title()?></a></h3>
		
		<?php include ASTRO_BLOG_TEMPLATE . 'widgets/post/element/meta.php' ?>

		
		<?php if($settings['excerpt']):?>
			<div class="at-post__content">
				<?php echo astro_the_content($settings['excerpt'])?>
			</div>
		<?php endif ?>

		<?php if ($settings['readmore']):?>
		<a href="<?php echo get_permalink()?>" class="rt-btn at-post__readmore"><?php echo $settings['readmore']?></a>
		<?php endif?>

	</div>
	</div>

</div>
