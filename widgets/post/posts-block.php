<?php
namespace Astro_Blog\Elementor;

use Astro_Blog\Helper;
use Astro_Blog\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts_Block extends Posts
{
    public function get_name()
    {
        return 'astro-posts-block';
    }

    public function get_title()
    {
        return __('Posts Block', 'astro-blog');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-block';

    }

    public function get_categories()
    {
        return ['astro-blog'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options(); //protected


        $this->style_general();
        $this->style_image_block();
        $this->style_title_block();
        $this->style_meta_block();
        $this->style_badges();
        $this->style_excerpt();

        $this->setting_button(array(
            'class' => '.at-post__readmore',
            'label' => 'Read More',
        ));
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-blog'),
            ]
        );

        $this->add_control(
            'pagination',
            [
                'label' => __('Pagination', 'astro-blog'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'none',
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image on Main Post', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'image_size_group',
            [
                'label' => __('Image On Group Post', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'excerpt',
            [
                'label' => __('Excerpt Number', 'astro-blog'),
                'type' => Controls_Manager::NUMBER,
                'default' => __('18', 'astro-blog'),
            ]
        );

        $this->add_control(
            'readmore',
            [
                'label' => __('Read More', 'astro-blog'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Read More', 'astro-blog'),
            ]
        );
           $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',

            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        // $this->add_control(
        //     'meta_view',
        //     [
        //         'label' => __('View Count', 'astro-blog'),
        //         'type' => Controls_Manager::SWITCHER,
        //         'default' => 'no',
        //         'label_on' => __('On', 'astro-blog'),
        //         'label_off' => __('Off', 'astro-blog'),
        //         'return_value' => 'yes',
        //     ]
        // );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

      

        $this->end_controls_section();
    }

    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Primary Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main,
                    {{WRAPPER}} .at-post-block__group' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                padding-right: calc({{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .at-post-block' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .at-post-block__main .flex-item' => 'margin-bottom: {{SIZE}}{{UNIT}}',
                ],

            ]
        );

        $this->add_responsive_control(
            'general_group_margin',
            [
                'label' => __('Group Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__group .at-post' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_image_block()
    {
        $this->start_controls_section(
            'style_image',
            [
                'label' => __('Image', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_responsive_control(
            'image_width',
            [
                'label' => __('Group Image Width', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 80,
                        'max' => 200,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__group .at-post__thumbnail' => 'width: {{SIZE}}{{UNIT}};',
                ],

            ]
        );
        $this->end_controls_section();

    }

    public function style_title_block()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'title_normal',
            [
                'label' => __('Normal', 'astro-blog'),
            ]
        );
        $this->add_control(
            'title_primary_color',
            [
                'label' => __('Primary Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__title a' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'title_group_color',
            [
                'label' => __('Group Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__group .at-post__title a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_hover',
            [
                'label' => __('Hover', 'astro-blog'),
            ]
        );
        $this->add_control(
            'title_primary_color_hover',
            [
                'label' => __('Primary Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__title a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'title_group_color_hover',
            [
                'label' => __('Group Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__group .at-post__title a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'label' => __('Primary Typography', 'astro-blog'),
                'selector' => '{{WRAPPER}} .at-post-block__main .at-post__title',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography_group',
                'label' => __('Group Typography', 'astro-blog'),
                'selector' => '{{WRAPPER}} .at-post-block__group .at-post__title',
            ]
        );

        $this->add_responsive_control(
            'title_primary_margin',
            [
                'label' => __('Primary Title Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );


        $this->add_responsive_control(
            'title_group_margin',
            [
                'label' => __('Group Title Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__group .at-post__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_meta_block()
    {
        $this->start_controls_section(
            'style_meta',
            [
                'label' => __('Meta', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );


        /* start title color */
        $this->start_controls_tabs('meta_tabs');
        $this->start_controls_tab(
            'meta_normal',
            [
                'label' => __('Normal', 'astro-blog'),
            ]
        );

        $this->add_control(
            'meta_primary_link',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__meta a,
                    {{WRAPPER}} .at-post-block__main .at-post__meta' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'meta_group_link',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__group .at-post__meta a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'meta_hover',
            [
                'label' => __('Hover', 'astro-blog'),
            ]
        );
        $this->add_control(
            'meta_primary_link_hover',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__meta a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_control(
            'meta_group_link_hover',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__group .at-post__meta a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_responsive_control(
            'meta_primary_margin',
            [
                'label' => __('Primary Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__meta' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );
        
        $this->add_responsive_control(
            'meta_group_margin',
            [
                'label' => __('Group Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__group .at-post__meta' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

     public function style_excerpt()
    {
        $this->start_controls_section(
            'style_excerpt',
            [
                'label' => __('Excerpt', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'excerpt_color',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'excerpt_typography',
                'selector' => '{{WRAPPER}} .at-post-block__main .at-post__content',
            ]
        );

        $this->add_responsive_control(
            'excerpt_margin',
            [
                'label' => __('Excerpt Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .at-post-block__main .at-post__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }


    protected function render()
    {
        $settings = $this->get_settings();
        $settings['class'] = 'at-post at-post--grid';

        /* Clasess */
        $classes[] = 'at-post-block flex flex-row flex-cols-md-6 flex-cols-sm-12';
        $post_classes = '';

        /* get query argument */
        $the_query = new \WP_Query(Helper::query($settings));

        /* Header */
        echo HTML::header_block(array(
            'id' => 'header_' . $this->get_id(),
            'class' => 'rt-header-block--' . $settings['header_style'],
            'title' => $settings['header_title'],
        ));

        /* Start LOOP */

        if ($the_query->have_posts()):

            echo HTML::open(array(
                'id' => 'block_' . $this->get_id(),
                'class' => $classes,
                'data-page' => 1,
                'data-max-paged' => $the_query->max_num_pages,
            ));

            echo HTML::open('at-post-block__main flex-item');
                echo HTML::open('flex flex-row');

                    $index = 1;

                    while ($the_query->have_posts()): $the_query->the_post();

                        if ($index < 2) {
                            include dirname(__FILE__) . '/posts-view.php';
                        }

                        $index++;
                    endwhile;

                    wp_reset_postdata();

                echo HTML::close();
            echo HTML::close();

            echo HTML::open('at-post-block__group flex-item');
                echo HTML::open('flex flex-row');

                $index = 1;
                while ($the_query->have_posts()): $the_query->the_post();

                    if ($index > 1) {
                        include dirname(__FILE__) . '/element/list-small.php';

                    }

                    $index++;
                endwhile;
                wp_reset_postdata();
                
                echo HTML::close();
            echo HTML::close();

            echo HTML::close(); // close wrapper

            wp_reset_postdata();
        else:
            _e('No Result', 'astro-blog');
        endif;
    }
}
