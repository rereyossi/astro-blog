<?php
$date_url   = esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')));
$post_classes = !empty($settings['class'])?$settings['class']:"";
?>
<div class="flex-item">
	<div id="<?php echo 'post-'.get_the_ID()?>" class="<?php echo $post_classes?>">

		<div class="at-post__thumbnail" style="background-image: url('<?php echo the_post_thumbnail_url($settings['image_size']) ?>')"></div>

		<div class="at-post__body">

			<?php if (get_the_category() && $settings['meta_category']):?>
				<div class="at-post__badges">
					<?php foreach (get_the_category() as $term):?>

					<a href="<?php echo get_category_link($term->term_id)?>" class="<?php echo $term->slug?>"><?php echo $term->name?></a>
					<?php endforeach?>
				</div>
			<?php endif?>

			<h3 class="at-post__title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h3>
			
			<?php include ASTRO_BLOG_TEMPLATE.'widgets/post/element/meta.php' ?>
			
		</div>
	</div>
</div>