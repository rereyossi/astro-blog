<?php
$date_url = esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')));
$author_url = get_author_posts_url(get_the_author_meta('ID'));
$avatar = get_avatar(get_the_author_meta('ID'), 30);
$avatar_url = get_the_author_meta('profile_avatar', get_the_author_meta('ID'));

?>

<div id="<?php echo 'post-'.get_the_ID()?>" class="rt-slider__item">

	<div class="rt-slider__thumbnail"  style="background-image: url('<?php echo the_post_thumbnail_url($settings['image_size']) ?>')"></div>
	<div class="rt-slider__inner">
		<div class="rt-slider__bg"></div>
		<div class="rt-slider__body">

			<?php if (get_the_category() && $settings['meta_category']): ?>
				<div class="at-post__badges">
					<?php foreach (get_the_category() as $term): ?>
					<a href="<?php echo get_category_link($term->term_id) ?>" class="<?php echo $term->slug ?>"><?php echo $term->name ?></a>
					<?php endforeach?>
				</div>
			<?php endif?>

			<h3 class="rt-slider__title"><a href="<?php the_permalink()?>"><?php the_title()?></a></h3>

			<?php include ASTRO_BLOG_TEMPLATE.'widgets/post/element/meta.php' ?>
			
			<div class="rt-slider__content">
				<?php if($settings['excerpt']): ?>
				<?php echo astro_the_content($settings['excerpt']) ?>
				<?php endif ?>
			</div>
			

		</div>
	</div>

</div>

