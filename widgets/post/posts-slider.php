<?php
namespace Astro_Blog\Elementor;

use Astro_Blog\Helper;
use Astro_Blog\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts_Slider extends Posts
{
    public function get_name()
    {
        return 'astro-posts-slider';
    }

    public function get_title()
    {
        return __('Posts Slider', 'astro-blog');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-slider';

    }

    public function get_categories()
    {
        return ['astro-blog'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options(); //protected

        $this->style_general();
        $this->style_caption();
        $this->style_slider_title();
        $this->style_meta();
        $this->style_content();
        $this->style_badges();
        $this->setting_slider();
    }

    public function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-blog'),
            ]
        );

        $this->add_responsive_control(
            'post_height',
            [
                'label' => __('Height', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 100,
                        'max' => 700,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%', 'vh'],

                'selectors' => [
                    '{{WRAPPER}} .rt-slider__item' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'large',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'excerpt',
            [
                'label' => __('Excerpt Number', 'astro-blog'),
                'type' => Controls_Manager::NUMBER,
                'default' => __('18', 'astro-blog'),
            ]
        );
        $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',

            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        // $this->add_control(
        //     'meta_view',
        //     [
        //         'label' => __('View Count', 'astro-blog'),
        //         'type' => Controls_Manager::SWITCHER,
        //         'default' => 'no',
        //         'label_on' => __('On', 'astro-blog'),
        //         'label_off' => __('Off', 'astro-blog'),
        //         'return_value' => 'yes',
        //     ]
        // );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->end_controls_section();
    }
    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'general_background',
                'selector' => '{{WRAPPER}} .rt-slider .rt-slider__bg',
            ]
        );

        $this->end_controls_section();
    }
    public function style_caption()
    {
        $this->start_controls_section(
            'style_caption',
            [
                'label' => __('Caption', 'astro_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'caption_background',
                'selector' => '{{WRAPPER}} .rt-slider__body',
            ]
        );
        $this->add_responsive_control(
            'caption_width',
            [
                'label' => __('Width', 'astro_domain'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__body' => 'max-width: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'caption_alignment',
            [
                'label' => __('Layout Alignment', 'astro_domain'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro_domain'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro_domain'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro_domain'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__inner' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'caption_horizontal_position',
            [
                'label' => __('Horizontal Position', 'astro_domain'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'default' => 'left',
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro_domain'),
                        'icon' => 'eicon-h-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro_domain'),
                        'icon' => 'eicon-h-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro_domain'),
                        'icon' => 'eicon-h-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__inner' => 'justify-content: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'caption_vertical_position',
            [
                'label' => __('Vertical Position', 'astro_domain'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'default' => 'end',
                'options' => [
                    'start' => [
                        'title' => __('Top', 'astro_domain'),
                        'icon' => 'eicon-v-align-top',
                    ],
                    'center' => [
                        'title' => __('Middle', 'astro_domain'),
                        'icon' => 'eicon-v-align-middle',
                    ],
                    'end' => [
                        'title' => __('Bottom', 'astro_domain'),
                        'icon' => 'eicon-v-align-bottom',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__inner' => 'align-items: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'caption_padding',
            [
                'label' => __('Padding', 'astro_domain'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%', 'em'],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_slider_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'title_normal',
            [
                'label' => __('Normal', 'astro-blog'),
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__title a' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_hover',
            [
                'label' => __('Hover', 'astro-blog'),
            ]
        );
        $this->add_control(
            'title_color_hover',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__title a:hover' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .rt-slider__title',
            ]
        );

        $this->add_responsive_control(
            'title_margin',
            [
                'label' => __('Title Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }
    public function style_content()
    {
        $this->start_controls_section(
            'style_excerpt',
            [
                'label' => __('Excerpt', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'excerpt_typography',
                'selector' => '{{WRAPPER}} .rt-slider__content',
            ]
        );

        $this->add_control(
            'excerpt_color',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'excerpt_margin',
            [
                'label' => __('Content Spacing', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();

        /* get query argument */
        $the_query = new \WP_Query(Helper::query($settings));

        /* Start LOOP */
        if ($the_query->have_posts()):

            echo HTML::before_slider(array(
                'id' => 'astro-slider-' . $this->get_id(),
                'class' => ["rt-slider js-astro-slider rt-slider--pagination-inner"],
                'items-lg' => 1,
                'items-md' => 1,
                'items-sm' => 1,
                'pagination' => $settings['slider_pagination'],
                'gap' => 0,
                'nav' => $settings['slider_nav'],
            ));

            // Header
            echo HTML::header_block(array(
                'class' => 'rt-header-block--' . $settings['header_style'],
                'title' => $settings['header_title'],
                'nav' => ($settings['slider_nav'] == 'header') ? true : false,
                'id' => 'header-' . $this->get_id(),
            ));

            echo HTML::open('rt-slider__main owl-carousel');

            while ($the_query->have_posts()): $the_query->the_post();

                include dirname(__FILE__) . '/posts-slider-view.php';

            endwhile;

            echo HTML::close();

            echo HTML::after_slider();

            wp_reset_postdata();

        else:
            _e('No Result', 'astro-blog');
        endif;
    }
}
