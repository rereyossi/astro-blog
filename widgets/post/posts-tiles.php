<?php
namespace Astro_Blog\Elementor;

use Astro_Blog\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts_Tiles extends Posts
{
    public function get_name()
    {
        return 'astro-posts-tiles';
    }

    public function get_title()
    {
        return __('Posts Tiles', 'astro-blog');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-tiles';

    }

    public function get_categories()
    {
        return ['astro-blog'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options(); //protected

        $this->style_general(); //protected
        $this->style_title();
        $this->style_badges();
        $this->style_meta();

        $this->setting_button(array(
            'name' => 'loadmore',
            'label' => 'Load More',
            'class' => '.rt-pagination__button',
        ));

        $this->setting_carousel();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-blog'),
            ]
        );

        $this->add_responsive_control(
            'post_height',
            [
                'label' => __('Height', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 250,
                ],
                'range' => [
                    'px' => [
                        'min' => 100,
                        'max' => 700,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],

                'selectors' => [
                    '{{WRAPPER}} .at-post__thumbnail' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'astro-blog'),
                    2 => __(2, 'astro-blog'),
                    3 => __(3, 'astro-blog'),
                    4 => __(4, 'astro-blog'),
                    6 => __(6, 'astro-blog'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

           $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',

            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'pagination_style',
            [
                'label' => __('Pagination Style', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'no_pagination',
                'options' => array(
                    'no_pagination' => 'No Pagination',
                    'loadmore' => 'Load More',
                ),
            ]
        );

        $this->end_controls_section();
    }

    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

 
        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'id' => $this->get_id(),
            'template_part' => 'widgets/post/posts-tiles-view',
            'class' => 'at-post at-post--tiles',
            'layout_masonry' => false,
        );
        
        echo  $this->elementor_loop(wp_parse_args($args, $settings));
    }
}
