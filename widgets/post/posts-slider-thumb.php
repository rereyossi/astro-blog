<?php
namespace Astro_Blog\Elementor;

use Astro_Blog\Helper;
use Astro_Blog\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts_Slider_Thumb extends Posts_Slider
{
    public function get_name()
    {
        return 'astro-posts-slider-thumb';
    }

    public function get_title()
    {
        return __('Posts Slider Thumbs', 'astro-blog');
    }

    public function get_icon()
    {
        return 'ate-icon ate-slider-thumbnail';

    }

    public function get_categories()
    {
        return ['astro-blog'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options(); //protected

        $this->style_thumbnail();
        $this->style_general();

        $this->style_caption();
        $this->style_slider_title();
        $this->style_meta();
        $this->style_content();
        $this->style_badges();

        $this->setting_slider_thumb();
    }

    public function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-blog'),
            ]
        );

        $this->add_responsive_control(
            'post_height',
            [
                'label' => __('Height', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 100,
                        'max' => 700,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', 'vh'],

                'selectors' => [
                    '{{WRAPPER}} .rt-slider__main .rt-slider__item' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image On Main Post', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'large',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'image_size_thumbnail',
            [
                'label' => __('Image On Group Post', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'thumbnail',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'excerpt',
            [
                'label' => __('Excerpt Number', 'astro-blog'),
                'type' => Controls_Manager::NUMBER,
                'default' => __('18', 'astro-blog'),
            ]
        );
        $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        // $this->add_control(
        //     'meta_view',
        //     [
        //         'label' => __('View Count', 'astro-blog'),
        //         'type' => Controls_Manager::SWITCHER,
        //         'default' => 'no',
        //         'label_on' => __('On', 'astro-blog'),
        //         'label_off' => __('Off', 'astro-blog'),
        //         'return_value' => 'yes',
        //     ]
        // );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->end_controls_section();
    }

    public function setting_slider_thumb()
    {
        $this->start_controls_section(
            'setting_slider',
            [
                'label' => __('Slider', 'astro-blog'),
            ]
        );

        $slides_to_show = range(1, 20);
        $slides_to_show = array_combine($slides_to_show, $slides_to_show);

        $this->add_responsive_control(
            'slider_item',
            [
                'label' => __('Slides Thumbnail to Show', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => '10',
                'options' => $slides_to_show,
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 10,
                'tablet_default' => 6,
                'mobile_default' => 6,

            ]
        );

        $this->add_control(
            'slider_gap',
            [
                'label' => __('Thumbnail Spacing', 'astro-blog'),
                'type' => Controls_Manager::NUMBER,
                'default' => 10,
                'min' => 0,
                'max' => 100,
                'step' => 1,
                'description' => __('spacing between Carousel item', 'astro-blog'),
            ]
        );

        $this->add_control(
            'slider_nav',
            [
                'label' => __('Navigation', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'none',
                'options' => array(
                    'none' => 'None',
                    'beside' => 'On Side',
                    'header' => 'On Header',
                ),
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'style_slider_navigation',
            [
                'label' => __('Slider', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'style_slider_nav',
            [
                'label' => __('Navigation', 'astro-blog'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        /* start navigation color */
        $this->start_controls_tabs('slider_nav_tabs');
        $this->start_controls_tab(
            'slider_nav_normal',
            [
                'label' => __('Normal', 'astro-blog'),
            ]
        );

        $this->add_control(
            'style_slider_navigation_color',
            [
                'label' => __('Navigation Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next i,
                         {{WRAPPER}}  .rt-slider .owl-prev i' => 'color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->add_control(
            'style_slider_navigation_bg',
            [
                'label' => __('Background Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next,
                         {{WRAPPER}} .rt-slider .owl-prev' => 'background-color: {{VALUE}} !important;',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'slider_nav_hover',
            [
                'label' => __('Hover', 'astro-blog'),
            ]
        );

        $this->add_control(
            'style_slider_navigation_color_hover',
            [
                'label' => __('Navigation Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next:hover i,
                         {{WRAPPER}}  .rt-slider .owl-prev:hover i' => 'color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->add_control(
            'style_slider_navigation_bg_hover',
            [
                'label' => __('Background Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next:hover,
                         {{WRAPPER}} .rt-slider .owl-prev:hover' => 'background-color: {{VALUE}} !important;',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_control(
            'style_slider_navigation_radius',
            [
                'label' => __('Border Radius', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next,
                     {{WRAPPER}} .rt-slider .owl-prev' => 'border-radius: {{SIZE}}{{UNIT}}!important;',
                ],
            ]
        );

        $this->add_responsive_control(
            'style_slider_navigation_position_top',
            [
                'label' => __('Top Indent', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-next,
                     {{WRAPPER}} .rt-slider .owl-prev' => 'top: {{SIZE}}{{UNIT}}!important;',
                ],
                'condition' => [
                    'slider_nav!' => 'header',
                ],
            ]
        );

        $this->add_responsive_control(
            'style_slider_navigation_position_side',
            [
                'label' => __('Side Indent', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 300,
                        'step' => 5,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider .owl-prev' => 'left: {{SIZE}}{{UNIT}}!important;',
                    '{{WRAPPER}} .rt-slider .owl-next' => 'right: {{SIZE}}{{UNIT}}!important;',
                ],
                'condition' => [
                    'slider_nav!' => 'header',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'style_slider_shadow',
                'selector' => '{{WRAPPER}} .rt-slider .owl-next,
                                   {{WRAPPER}} .rt-slider .owl-prev',
            ]
        );

        $this->end_controls_section();

    }



    public function style_thumbnail()
    {
        $this->start_controls_section(
            'style_thumbnail',
            [
                'label' => __('Thumbnail', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_responsive_control(
            'thumbnail_alignment',
            [
                'label' => __('Layout Alignment', 'astro-blog'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro-blog'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-blog'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro-blog'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__group' => 'display: flex; justify-content: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_section();
    }

    public function style_content()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'selector' => '{{WRAPPER}} .rt-slider__content',
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => __('Color', 'astro-blog'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'content_spacing',
            [
                'label' => __('Content Spacing', 'astro-blog'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-slider__content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();

        // Header
        echo HTML::header_block(array(
            'class' => 'rt-header-block--' . $settings['header_style'],
            'title' => $settings['header_title'],
            'nav' => ($settings['slider_nav'] == 'header') ? true : false,
            'id' => 'header-' . $this->get_id(),
        ));


        /* get query argument */
        $the_query = new \WP_Query(Helper::query($settings));

        /* Start LOOP */
        if ($the_query->have_posts()):

            echo HTML::before_slider(array(
                'id' => 'astro-slider-' . $this->get_id(),
                'items-lg' => $settings['slider_item'],
                'items-md' => $settings['slider_item_tablet'],
                'items-sm' => $settings['slider_item_mobile'],
                'gap' => $settings['slider_gap'],
                'nav' => ($settings['slider_nav'] != 'none' && $settings['slider_nav'] != 'header') ? true : false,
                'sync' => true,
            ));

            echo HTML::open('rt-slider__main owl-carousel');

            while ($the_query->have_posts()): $the_query->the_post();

                include dirname(__FILE__) . '/posts-slider-view.php';

            endwhile;
            wp_reset_postdata();

            echo HTML::close();

            //syn
            echo HTML::open('rt-slider__group owl-carousel');

            while ($the_query->have_posts()): $the_query->the_post();

                the_post_thumbnail($settings['image_size_thumbnail']);

            endwhile;
            wp_reset_postdata();

            echo HTML::close();

            echo HTML::after_slider();

        else:
            _e('No Result', 'astro-blog');
        endif;
    }
}
