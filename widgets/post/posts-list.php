<?php
namespace Astro_Blog\Elementor;

use Astro_Blog\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts_List extends Posts
{
    public function get_name()
    {
        return 'astro-posts-list';
    }

    public function get_title()
    {
        return __('Posts List', 'astro-blog');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-list';

    }

    public function get_categories()
    {
        return ['astro-blog'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options(); //protected


        $this->style_general(); //protected
        $this->style_image();
        $this->style_body();
        $this->style_title();
        $this->style_excerpt();
        $this->style_badges();
        $this->style_meta();

        $this->setting_button(array(
            'name' => 'readmore',
            'class' => '.at-post__readmore',
            'label' => 'Read More',
        ));

        $this->setting_button(array(
            'name' => 'loadmore',
            'label' => 'Load More',
            'class' => '.rt-pagination__button',
        ));

        $this->setting_carousel(array(
            'slide' => false,
            'gap' => false,
        ));
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-blog'),
            ]
        );
        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'excerpt',
            [
                'label' => __('Excerpt Number', 'astro-blog'),
                'type' => Controls_Manager::NUMBER,
                'default' => __('18', 'astro-blog'),
            ]
        );

        $this->add_control(
            'readmore',
            [
                'label' => __('Read More Text', 'astro-blog'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Read More', 'astro-blog'),
            ]
        );

           $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        // $this->add_control(
        //     'meta_view',
        //     [
        //         'label' => __('View Count', 'astro-blog'),
        //         'type' => Controls_Manager::SWITCHER,
        //         'default' => 'no',
        //         'label_on' => __('On', 'astro-blog'),
        //         'label_off' => __('Off', 'astro-blog'),
        //         'return_value' => 'yes',
        //     ]
        // );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'astro-blog'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-blog'),
                'label_off' => __('Off', 'astro-blog'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'pagination_style',
            [
                'label' => __('Pagination Style', 'astro-blog'),
                'type' => Controls_Manager::SELECT,
                'default' => 'no_pagination',
                'options' => array(
                    'no_pagination' => 'No Pagination',
                    'loadmore' => 'Load More',
                ),
            ]
        );

        $this->end_controls_section();
    }

    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-blog'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro-blog'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );


        $this->end_controls_section();
    }

  

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'id' => $this->get_id(),
            'template_part' => 'widgets/post/posts-view',
            'class' => 'at-post at-post--list',
            'setting_column' => 1,
            'setting_column_tablet' => 1,
            'setting_column_mobile' => 1,
            'layout_masonry' => false,
            'slider_item' => 1,
            'slider_item_tablet' => 1,
            'slider_item_mobile' => 1,
            'slider_gap' => 1,

        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));

    }
}
