<?php

namespace Astro_Blog\Admin;

class Options
{

    public function __construct()
    {
        add_action('admin_menu', array($this, 'add_menu_panel'));

    }

    /**
     * Show plugin menu
     *
     * @return void
     */
    public function add_menu_panel()
    {

        add_submenu_page('options-general.php', 'Astro Blog', 'Astro Blog', 'edit_posts', 'astro_blog', function(){
            include ASTRO_BLOG_TEMPLATE . 'core/admin/views/license.php';
        });

    }


}

new Options;
